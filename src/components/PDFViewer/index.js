import { useEffect, useRef } from "react";
//
import { init } from "./init";

const PDFViewer = () => {
  const viewer = useRef(null);

  useEffect(() => {
    init({ reference: viewer }).then((instance) => {
      console.log(instance);
    });
  }, []);

  return (
    <div
      id="mywebviewer"
      className="webviewer"
      ref={viewer}
      style={{ height: "100vh" }}
    ></div>
  );
};

export default PDFViewer;
