import WebViewer, { getInstance } from "@pdftron/webviewer";

const init = async ({ reference }) => {
  let instance = getInstance();
  if (instance) {
    console.log("Previous instance", instance);
    // RE-INJECT THIS INSTANCE IN REFERENCE TO RELIEVE MEMORY
  } else {
    instance = await WebViewer(
      {
        path: "/webviewer/lib",
        initialDoc: "/files/plan_architecture_002.pdf",
      },
      reference.current
    );
  }
  return instance;
};

export { init };
