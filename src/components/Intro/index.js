import Typography from "@mui/material/Typography";
import Box from "@mui/material/Box";

const Intro = () => (
  <Box sx={{ p: 3 }}>
    <Typography variant="h3">Intro</Typography>
    <Typography>
      Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed sollicitudin
      augue ipsum, nec efficitur turpis molestie vel. Curabitur semper eget
      massa quis rutrum. Fusce ultrices condimentum tempus. Donec rhoncus nisl
      et eros dapibus convallis. Fusce efficitur dui sodales augue consectetur
      bibendum.
      <br />
      Curabitur arcu ipsum, facilisis non nisi eget, semper dictum diam. Morbi
      sollicitudin, libero ut aliquet vulputate, est magna pretium eros, eu
      tincidunt risus neque at nunc. Sed semper feugiat augue at viverra. Donec
      quis volutpat lorem, eu vulputate turpis. Fusce et pretium elit. Aliquam
      at nulla a nisi sodales ultricies ac sed erat.
    </Typography>
  </Box>
);

export default Intro;
